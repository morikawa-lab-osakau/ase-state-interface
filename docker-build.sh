# docker build \
#     --build-arg state_src='./state-5.6.14.tgz' \
#     -t "ase-test-interface-test" \
#     .


STATE_VERSION=5.6.10
docker build \
    -f docker/Dockerfile \
    -t registry.gitlab.com/morikawa-lab-osakau/ase-state-interface:state-$STATE_VERSION \
    --no-cache \
    --build-arg STATE_VERSION=$STATE_VERSION \
    .

STATE_VERSION=5.6.14
docker build \
    -f docker/Dockerfile \
    -t registry.gitlab.com/morikawa-lab-osakau/ase-state-interface:state-$STATE_VERSION \
    --no-cache \
    --build-arg STATE_VERSION=$STATE_VERSION \
    .




